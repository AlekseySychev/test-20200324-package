<?php

namespace AlekseySychev;

Class Template
{
    private $vars = [];
    private $template = null;
    private $template_file = null;
    private $expressions = [
        '{if [ ]*(.*?)[ ]*}' => 'if ({$1}):',
        '{else}' => 'else:',
        '{/if}' => 'endif;',
    ];
    private $cache_dir = null;

    public function __construct()
    {
        $this->cache_dir = sys_get_temp_dir();
    }

    public function setVar($name = null, $value = null)
    {
        $this->vars[$name] = $value;
    }

    public function setTemplate($name = null)
    {
        $this->template = file_get_contents($name);
        $this->template_file = $name;
    }

    public function show()
    {
        $cache_file = $this->cache_dir . '/' . md5($this->template) . '.php';
        $template_file = $this->template_file;

        if (!is_file($cache_file) || filemtime($cache_file) <= filemtime($template_file)) {
            $output = $this->template;

            // only stupid vars
            foreach ($this->vars as $name => $value) {
                $output = str_replace('{' . $name . '}', '<?=$' . $name . ';?>', $output);
            }

            preg_match_all('#\{.*?\}#', $output, $all);
            foreach ($all[0] as $key => $item) {
                $new = $item;

                foreach ($this->expressions as $expression_regexp => $expression_replace) {
                    $new = preg_replace('#' . $expression_regexp . '#', $expression_replace, $new);
                }

                foreach ($this->vars as $name => $value) {
                    $new = str_replace($name, '$' . $name, $new);
                }

                $new = str_replace(['{', '}'], '', $new);

                $output = str_replace($item, '<?php ' . $new . ' ?>', $output);
            }

            file_put_contents($cache_file, $output);
        }

        foreach ($this->vars as $name => $value) {
            $$name = $value;
        }

        try {
            ob_start();
            include($cache_file);
            echo ob_get_clean();
        } catch (\Exception $e) {

        }

    }
}