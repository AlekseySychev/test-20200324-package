<?php
use PHPUnit\Framework\TestCase;
use AlekseySychev\Template;

class VarTest extends TestCase
{
    /**
     * @dataProvider additionProvider
     */
    public function testVar($var, $result)
    {
        $template = new Template();

        $template->setVar('var', $var);
        $template->setTemplate(__DIR__ . '/template.tpl');
        $template->show();

        $this->expectOutputString($result);
    }

    public function additionProvider()
    {
        $tests = [];
        for ($i = 0; $i < 10; $i++)
        {
            $var = rand();
            $result = '-' .$var .'-' ;
            $tests[] = [
                'var' => $var,
                'result' => $result,
            ];
        }
        return $tests;
    }
}