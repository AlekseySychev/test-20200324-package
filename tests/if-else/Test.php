<?php
use PHPUnit\Framework\TestCase;
use AlekseySychev\Template;

class IfElseTest extends TestCase
{
    /**
     * @dataProvider additionProvider
     */
    public function testIfElse($a, $b, $result)
    {
        $template = new Template();

        $template->setVar('a', $a);
        $template->setVar('b', $b);


        $template->setTemplate(__DIR__ . '/template.tpl');
        $template->show();

        $this->expectOutputString($result);
    }

    public function additionProvider()
    {
        $tests = [];
        for ($i = 0; $i < 10; $i++)
        {
            $a = rand();
            $b = rand();
            $result = $a>$b? 'a>b':'b>=a';
            $tests[] = [
                'a' => $a,
                'b' => $b,
                'result' => $result,
            ];
        }
        return $tests;
    }
}